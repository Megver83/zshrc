# Custom Prompt Shell
#
# Copyright (c) 2017 Jesús E. et al
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ~/.bashrc
#
# Desarrollador: Jesús E. <heckyel@hyperbola.info>
# Sitio Web: https://heckyel.ga
#
# Contribuidor: David P. <megver83@parabola.nu>
# Sitio web: https://megver83.ga
#

# bash completion: pacman -S bash-completion
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

xhost +local:root > /dev/null 2>&1

# Agrega archivo aliases
[ -f ~/.bash_aliases ] && source ~/.bash_aliases || true

# Agrega archivo de funciones
[ -f ~/.bash_functions ] && source ~/.bash_functions || true

# pkgfile: pacman -S pkgfile
[ -f /usr/share/doc/pkgfile/command-not-found.bash ] && source /usr/share/doc/pkgfile/command-not-found.bash || true

# Autocompletado
# complete -cf sudo
# 
# shopt -s cdspell
# shopt -s checkwinsize
# shopt -s cmdhist
# shopt -s dotglob
# shopt -s expand_aliases
# shopt -s extglob
# shopt -s histappend
# shopt -s hostcomplete

export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoreboth
export JAVA_FONTS=/usr/share/fonts/TTF
export EDITOR=/usr/bin/nano

# Variable de Entorno para Sass
# Sass
# export PATH+=:~/.gem/ruby/2.4.0/bin
_ruby="$(ruby -rubygems -e "puts Gem.user_dir")/bin"
PATH+=:$_ruby

#---------------
# Theme's prompt
#---------------

# Theme 0
theme_0="\n${NC}${NEGRITA_AMARILLO}[ \u ${NC}${NEGRITA_AZUL}| $(date +%d-%m-%Y) | ${NEGRITA_ROJO}\@ ]${NEGRITA_BLANCO}\n[\$PWD] \$(exitstatus) \$(git_prompt)${NC}\n\\[$ST_VERDE\]$\[$NC\] "
# Theme 1
theme_1="\n${ST_CYAN}┌─${NEGRITA_AMARILLO}\u${NEGRITA_CYAN}@${NEGRITA_GRIS}\h \$(exitstatus) ${NEGRITA_BLANCO}[\$PWD] \$(git_prompt) ${NC}\n\\[$ST_CYAN\]╰─➤${NEGRITA_VERDE}$\[$NC\] "
# Theme 2
minterm="\n${ST_CYAN}┌─${NEGRITA_AMARILLO}\u${NEGRITA_CYAN}@${NEGRITA_GRIS}\h \$(exitstatus) ${NEGRITA_BLANCO}[\$PWD] \$(git_prompt) ${NC}\n\\[$ST_CYAN\]╰─➤ ${NEGRITA_GRIS}\$(date +%H:%M) ${NEGRITA_VERDE}$\[$NC\] "
# Theme 3
pure="${NEGRITA_VERDE}\u ${NEGRITA_AMARILLO}[$NEGRITA_ROJO\w$NEGRITA_AMARILLO] \$(exitstatus) ${NEGRITA_AZUL}(\$(date +%H:%M:%S)) \$(git_prompt)\[$ST_CYAN\]→\[$NC\] "

# Agrega archivo de bashrc_custom
[ -f ~/.zshrc_custom ] && source ~/.zshrc_custom || true

#---------------
# Shell prompt
#---------------

if [ -f ~/.bash_functions ] && [ -f ~/.zshrc_custom ]; then
    PS1=$prompt
elif [ -f ~/.bash_functions ]; then
    PS1=$theme_0
else
    PS1="[\u@\h \W]\$ "
fi

    #Prompt interactivo
    PS2="$NEGRITA_CYAN=>\[$NC\] "

    # Establecer Puertos para Reproduccion Midi
    # Si Tienes Instalado y estas Usando " Timidity / Timidity++ " :
    export ALSA_OUTPUT_PORTS="128:0","128:1","128:2","128:3"
